import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import Main.Grid;

public class Main extends JFrame
{
    public static void main(String[] args) throws Exception 
    {
        Main window = new Main();
    }

    public Main()
    {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        this.setContentPane(canvas);
        this.pack();
        this.setVisible(true);
    }
}