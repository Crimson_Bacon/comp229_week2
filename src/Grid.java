import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import Main.Cell;

public class Main extends JFrame 
{
    public class Canvas extends JPanel
    {
        public Canvas()
        {
            setPreferredSize(new Dimension(720, 720));
        }

        @Override
        public void paint(Graphics g)
        {
            for(int i = 10; i < 710; i+=35)
            {
                for(int k = 10; k < 710; k+=35)
                {
                    g.setColor(Color.WHITE);
                    g.fillRect(i, k, 35, 35);
                    g.setColor(Color.BLACK);
                    g.drawRect(i, k, 35, 35);
                }
            }
        }
    }
    
}